import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { PopinfoComponent } from 'src/app/components/popinfo/popinfo.component';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.page.html',
  styleUrls: ['./popover.page.scss'],
})
export class PopoverPage implements OnInit {

  constructor(private popOverCrtl: PopoverController) { }

  ngOnInit() {
  }

  async mostrarPop(evento) {
    const popOver = await this.popOverCrtl.create({
      component: PopinfoComponent,
      event: evento,
      mode: 'ios',
      backdropDismiss: false
    });
    await popOver.present();

    const { data } = await popOver.onWillDismiss();

    console.log('Padre:', data);


  }

}
