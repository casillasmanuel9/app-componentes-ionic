import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { pairs } from 'rxjs';

@Component({
  selector: 'app-modal-info',
  templateUrl: './modal-info.page.html',
  styleUrls: ['./modal-info.page.scss'],
})
export class ModalInfoPage implements OnInit {

  @Input() nombre;
  @Input() pais;

  constructor(private modalCrtl: ModalController) { }

  ngOnInit() {
  }

  salirSinArgumentos() {
    this.modalCrtl.dismiss();
  }

  salirConArgumentos() {
    this.modalCrtl.dismiss({
      nombre: 'felipe',
      pais: 'españa'
    });
  }

}
